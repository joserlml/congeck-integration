import { Auth } from "../Middleware/Auth";
import { UserModel, } from "../Models/UserModel";
import { UserProfileModel, UserProfileInterface } from "../Models/UserProfileModel";
import { CoingeckoHelpter } from "../_helpers/coingecko_helper";


const coingeckoHelpter = new CoingeckoHelpter();
const auth = new Auth();
const userModel = new UserModel();
const userProfileModel = new UserProfileModel();

export class CoinController {

    constructor() {

    }
    /**
     * Controlador para obtener una lista de criptomonedas
     */
    GetList(requests: any, response: any) {
        if (requests.params.limit &&
            requests.params.limit < 0) {
            response.status(400).send({})
        } else {
            coingeckoHelpter.GetCoins(
                [],
                (requests.authUser ? requests.authUser.data.currency : "usd"),
                (requests.params.limit || 9999999999999),
                (requests.params.ord && requests.params.ord == "asc" ? coingeckoHelpter.ORD_CAP_ASC : coingeckoHelpter.ORD_CAP_DESC)
            ).then(data => {
                //  console.log(data)
                response.send(data)
            }).catch(err => {
                //console.log(err)
                response.status(500).send({})
            })
        }
    }
    /**
     * Controlador para obtener una lista de criptomonedas segun preferencia del usuario
     */
    GetListCoinByUser(requests: any, response: any) {
        if (isNaN(requests.params.limit) || requests.params.limit > 25 ||
            requests.params.limit < 0) {
            response.status(400).send({})
        } else {
            userProfileModel.GetUserProfile(requests.authUser.data).then((resposnedb: any) => {
                coingeckoHelpter.GetCoinsByUser(
                    (resposnedb ? resposnedb.map((x: UserProfileInterface) => x.id_coin) : ["undefined"]),
                    requests.params.limit,
                    (requests.params.ord && requests.params.ord == "asc" ? coingeckoHelpter.ORD_CAP_ASC : coingeckoHelpter.ORD_CAP_DESC)
                ).then((data: any) => {
                    let dataresponse = [];
                    for (const coin of data) {
                        if (dataresponse.length < requests.params.limit) {
                            dataresponse.push(coin)
                        } else {
                            break
                        }
                    }
                    response.send(dataresponse)
                }).catch(err => {
                    // console.log(err)
                    response.status(500).send({})
                })
            }).catch((err) => {
                //    console.log(err)
                response.status(500).send({})

            })
        }
    }


}