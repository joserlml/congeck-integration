
import * as bcrypt from 'bcrypt';
import { CoingeckoHelpter } from '../_helpers/coingecko_helper';
import { UserProfileModel } from '../Models/UserProfileModel';

const userProfileModel = new UserProfileModel();
const coingeckoHelpter = new CoingeckoHelpter();

export class UserProfileController {

    constructor() {
    }

    /**
     * Controlador para agregar monedas preferidas del usuario
     */
    PostUserProfile(requests: any, response: any) {
        if (typeof (requests.body.id_coin) != "string" || typeof (requests.authUser) != "object") {
            response.status(400).send({})
        } else {
            userProfileModel.ValidateUserAndCoin({ id_user: requests.authUser.data.id, id_coin: requests.body.id_coin }).then((exist) => {
                if (exist) {
                    response.status(409).send({})
                } else {
                    coingeckoHelpter.ValidateIdCoin(requests.body.id_coin).then(() => {
                        userProfileModel.InsertCoin({ id_coin: requests.body.id_coin, id_user: requests.authUser.data.id }).then(responseDB => {
                            response.send({})
                        }).catch(err => {
                            //   console.log(err)
                            response.status(500).send({})
                        })
                    }).catch((err) => {
                        if (err) {
                            response.status(404).send({})
                        } else {
                            response.status(500).send({})
                        }
                    })
                }
            }).catch((err) => {
                response.status(500).send({})
            })
        }
    }
}
