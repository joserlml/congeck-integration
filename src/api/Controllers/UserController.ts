import { UserModel, UserInterface } from '../Models/UserModel';
import * as bcrypt from 'bcrypt';
import { CoingeckoHelpter } from '../_helpers/coingecko_helper';

const userModel = new UserModel();
const coingeckoHelpter = new CoingeckoHelpter();

export class UserController {

    constructor() {
    }

    /*
    * Controlador para registro de usuario
    */
    PostUser(requests: any, response: any) {
        if (typeof (requests.body.username) != "string" || typeof (requests.body.password) != "string"
            || typeof (requests.body.name) != "string" || typeof (requests.body.lastname) != "string"
            || typeof (requests.body.currency) != "string" || requests.body.password.length < 8) {
            response.status(400).send({})
        } else {
            coingeckoHelpter.ValidateCurrency(requests.body.currency).then((existCurrency) => {
                if (existCurrency) {
                    userModel.ValidateUserName(requests.body).then((validateUser) => {
                        if (!validateUser) {
                            bcrypt.hash(requests.body.password, parseInt(<any>process.env.BCRYPT_SALT_ROUNDS_PASSWORD)).then(hashpassword => {
                                requests.body.password = hashpassword;
                                userModel.InsertReturnId(requests.body).then(responseDB => {
                                    response.send({})
                                }).catch(err => {
                                    response.status(500).send({})
                                })
                            }).catch(err => {
                                console.log(err)
                                console.error("Error generando el hash")
                            })
                        } else {
                            response.status(409).send({})
                        }
                    }).catch(err => {
                        console.error(err)
                        response.status(500).send({})
                    })
                } else {
                    response.status(404).send({})
                }
            }).catch(err => {
                console.error(err)
                response.status(500).send({})
            })
        }
    }

}
