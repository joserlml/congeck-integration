import { Auth } from "../Middleware/Auth";
import { UserInterface, UserModel } from "../Models/UserModel";

const userModel = new UserModel();
const auth = new Auth();

export class LoginController {
    constructor() {

    }

    /**
     * Controlador para el ingreso del usuario
     */
    Login(requests: any, response: any) {
        if (typeof (requests.body.username) != "string" && typeof (requests.body.password) != "string") {
            response.status(400).send({})
        } else {
            let user: UserInterface = <any>{};
            user.username = requests.body.username;
            userModel.SelectByUserId(user).then((responsedb: any) => {
                if (!responsedb || !responsedb.id) {
                    response.status(401).send({})
                } else {
                    auth.login(requests.body.password, responsedb).then((responseAuth: any) => {
                        if (responseAuth.success) {
                            response.send(responseAuth);
                        } else {
                            response.status(401).send({})
                        }
                    })
                }
            })
        }
    }

    /**
     * Controlador para validar el ingreso del usuario
     */
    Validate(requests: any, response: any, next: any) {
        if (typeof (requests.headers.authorization) != "string") {
            response.status(406).send({})
        } else {
            auth.validate(requests.headers.authorization).then((response) => {
                requests.authUser = response
                next();
            }).catch((error) => {
                // console.log(error)
                response.status(401).send({})
            })
        }
    }
}