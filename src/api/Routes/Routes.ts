import express, { request, response } from 'express'
import bodyParser from 'body-parser';

import { UserController } from '../Controllers/UserController';
import { LoginController } from '../Controllers/LoginController';
import { CoinController } from '../Controllers/CoinController'
import { UserProfileController } from '../Controllers/UserProfileController'

const router = express.Router();
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
router.use(bodyParser.raw());


/**
 * Endpoint del login y registro
 **/

const loginController = new LoginController();

// endporint para ingresar al sistema
router.post("/api/login", (requests, response) => {
    loginController.Login(requests, response)
})

const userController = new UserController();

// endpoint para registrar un usuario
router.post("/api/user/register", (requests, response) => {
    userController.PostUser(requests, response)
})




/**
 * Endpoint de criptomonedas (Sin validacion de usuario)
 **/

const coinController = new CoinController();

// endpoint para obtener N cantidad de informacion de criptomonedas y en un orden dado
router.get("/api/coin/list/:limit?/:ord?", coinController.GetList.bind(this))




/**
 * Endpoint para validacion de token de usuario (desde este punto para abajo se validar el token del usuario) 
 **/
router.all("*", (req, res, next) => {
    loginController.Validate(req, res, next)
})

/**
 * Endpoint de criptomonedas (Con validacion de usuario)
 **/


// endpoint para buscar las monedas segun las configuraciones del usuario
router.get("/api/coin/user/list/:limit/:ord?", coinController.GetListCoinByUser.bind(this))



/**
 * Endpoint de perfil del usuario
 **/

const userProfileController = new UserProfileController();

// endpoint para registrar una criptomoneda a un usuario
router.post("/api/user/profile/coin", userProfileController.PostUserProfile.bind(this));



export { router }