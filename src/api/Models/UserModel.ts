import { Sqlite3Database } from "../Database/SqliteDatabase";

export interface UserInterface {
    id: number;
    username: string;
    password: string;
    name: string;
    lastname: string;
    currency: string;
}

export class UserModel {
    private database: Sqlite3Database;
    constructor() {
        this.database = new Sqlite3Database();
    }

    /**
     * Se valida que el "username" de un usuario exista
     */
    ValidateUserName(user: UserInterface) {
        return new Promise((resolve, reject) => {
            if (this.database.connected) {
                this.database.select("select id from user where username =?", [user.username]).then((response: any) => {
                    if (response.length > 0) {
                        resolve(true)
                    }
                    resolve(false)

                }).catch((err: any) => {
                    console.error(err)
                    resolve(false)
                })
            } else {
                reject({ status: false, message: "error de conexion" })
            }
        })
    }

    /**
     * Se inserta un nuevo usuario a la base de datos
     */
    InsertReturnId(user: UserInterface) {
        return new Promise((resolve, reject) => {
            if (this.database.connected) {
                this.database.excute('INSERT INTO user (username,password,name,lastname,currency) VALUES (?,?,?,?,?)', [user.username, user.password, user.name, user.lastname, user.currency]).then((response: any) => {
                    resolve(response);
                }).catch((err: any) => {
                    console.error(err)
                    resolve({ user })
                })

                resolve({})
            } else {
                reject({ status: false, message: "error de conexion" })
            }
        })
    }
    
    /**
     * Se obtiene un usuario por su id
     */
    SelectByUserId(userReq: UserInterface) {
        return new Promise((resolve, reject) => {
            let user: UserInterface;
            if (this.database.connected) {
                this.database.select("select * from user where username =?", [userReq.username]).then((response: any) => {
                    if (response.length > 0) {
                        user = response[0];
                    }
                    resolve(user)
                }).catch((err) => {
                    console.error(err)
                    resolve(user)
                })
            } else {
                reject({ status: false, message: "error de conexion" })
            }
        })
    }

}