import { Sqlite3Database } from "../Database/SqliteDatabase";

export interface AuthInterface {
    id_user: number;
    token: string;
}


export class AuthModel {
    private database: Sqlite3Database;
    constructor() {
        this.database = new Sqlite3Database();
    }

    /**
     * Funcion para insertar un token de ingreso de usuario al sistema
     */
    InsertToken(auth: AuthInterface) {
        return new Promise((resolve, reject) => {
            if (this.database.connected) {
                this.database.excute('INSERT INTO auth ("id_user","token") VALUES (?,?)', [auth.id_user, auth.token]).then((response: any) => {
                    resolve(response);
                }).catch((err: any) => {
                    console.error(err)
                    reject({ status: false, message: "error con la consulta" })
                })

                resolve({})
            } else {
                reject({ status: false, message: "error de conexion" })
            }
        })
    }

    /**
     * Funcion para seleccionar un token de acceso si no ha pasado mas de 24h
     */
    SelectByUser(id_user: number) {
        return new Promise((resolve, reject) => {
            let auth: AuthInterface;
            if (this.database.connected) {
                this.database.select("select *,(julianday('now') - julianday(created_at))* 24 as diff from auth where  (julianday('now') - julianday(created_at))* 24 <=" + <string>process.env.TOKEN_LIFETIME + " AND id_user =? ", [id_user]).then((response: any) => {
                    if (response.length > 0) {
                        auth = response[0];
                    }
                    resolve(auth)
                }).catch((err) => {
                    console.error(err)
                    reject({ status: false, message: "error con la consulta" })

                })
            } else {
                reject({ status: false, message: "error de conexion" })
            }
        })
    }

}