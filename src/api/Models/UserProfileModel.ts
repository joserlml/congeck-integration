import { Sqlite3Database } from "../Database/SqliteDatabase";
import { UserInterface } from "./UserModel";

export interface UserProfileInterface {
    id_user: number;
    id_coin: number;
}


export class UserProfileModel {
    private database: Sqlite3Database;
    constructor() {
        this.database = new Sqlite3Database();
    }

    /*
    * Se comprueba si la criptomoneda este agregada a un usuario
    */
    ValidateUserAndCoin(userProfile: UserProfileInterface) {
        return new Promise((resolve, reject) => {
            if (this.database.connected) {
                this.database.select("select id from userProfile where id_user =? AND id_coin=?", [userProfile.id_user, userProfile.id_coin]).then((response: any) => {
                    if (response.length > 0) {
                        resolve(true)
                    }
                    resolve(false)

                }).catch((err: any) => {
                    console.error(err)
                    resolve(false)
                })
            } else {
                reject({ status: false, message: "error de conexion" })
            }
        })
    }

    /*
    * Se agrega una criptomoneda a un usuario
    */
    InsertCoin(userProfile: UserProfileInterface) {
        return new Promise((resolve, reject) => {
            if (this.database.connected) {
                this.database.excute('INSERT INTO userProfile ("id_user","id_coin") VALUES (?,?)', [userProfile.id_user, userProfile.id_coin]).then((response: any) => {
                    resolve(response);
                }).catch((err: any) => {
                    console.error(err)
                    resolve({})
                })

                resolve({})
            } else {
                reject({ status: false, message: "error de conexion" })
            }
        })
    }

    /*
    * Se obtienen las criptomonedas segun el id del usuario
    */
    GetUserProfile(userReq: UserInterface) {
        return new Promise((resolve, reject) => {
            let userProfile: UserProfileInterface[];
            if (this.database.connected) {
                this.database.select("select * from userprofile where id_user =?", [userReq.id]).then((response: any) => {
                    if (response.length > 0) {
                        userProfile = response;
                    }
                    resolve(userProfile)
                }).catch((err) => {
                    console.error(err)
                    resolve([])
                })
            } else {
                reject({ status: false, message: "error de conexion" })
            }
        })
    }


}