import Axios from "axios";


export class CoingeckoHelpter {
    ORD_CAP_ASC: string = "market_cap_asc";
    ORD_CAP_DESC: string = "market_cap_desc";
    constructor() {
    }
    /*
    * Ping al api de coingeck
    */
    Ping() {
        return new Promise((resolve, reject) => {
            Axios.get("https://api.coingecko.com/api/v3/ping", { timeout: 1800 }).then((response) => {
                resolve(true)
            }).catch((err) => {
                reject(false)
            })
        })
    }

    /*
    * Obtiene las criptomonedas disponibles
    */
    GetCoins(ids: string[] = [], currency: string = "usd", limit: number = 100, ord: string = this.ORD_CAP_ASC) {
        return new Promise((resolve, reject) => {
            this.Ping().then(() => {
                Axios.get("https://api.coingecko.com/api/v3/coins/markets?" + (ids.length > 0 ? "ids=" + ids.join("%2C") : "") + "&vs_currency=" + currency + "&order=" + ord + "&per_page=" + limit + "&sparkline=false", { timeout: 1800 }).then((response) => {
                    const data = []
                    for (const coin of response.data) {
                        data.push({
                            'symbol': coin.symbol,
                            'price': coin.current_price,
                            'name': coin.name,
                            'image': coin.image,
                            'last_updated': coin.last_updated
                        })
                    }
                    resolve(data)
                }).catch(() => {
                    reject();
                })
            }).catch((err) => {
              //  console.log(err)
                reject()
            })
        })
    }

    /*
    * Obtiene las criptomonedas segun configuracion del usuario
    */
    GetCoinsByUser(ids: string[] = [], limit: number = 100, ord: string = this.ORD_CAP_ASC) {
        return new Promise((resolve, reject) => {
            this.Ping().then(() => {
                Axios.get("https://api.coingecko.com/api/v3/coins/markets?" + (ids.length > 0 ? "ids=" + ids.join("%2C") : "") + "&vs_currency=usd&order=" + ord + "&per_page=" + limit + "&sparkline=false", { timeout: 1800 }).then(async (response) => {
                    if (response.data) {
                        Axios.get("https://api.coingecko.com/api/v3/simple/price?ids=" + (ids.length > 0 ? ids.join("%2C") : "") + "&vs_currencies=eur%2Cars", { timeout: 1800 }).then((prices) => {
                            const data = []
                            for (const coin of response.data) {
                                if (prices.data) {
                                    data.push({
                                        'symbol': coin.symbol,
                                        'usd': coin.current_price,
                                        'eur': (prices.data[coin.id].eur ? prices.data[coin.id].eur : 0),
                                        'ars': (prices.data[coin.id].ars ? prices.data[coin.id].ars : 0),
                                        'name': coin.name,
                                        'image': coin.image,
                                        'last_updated': coin.last_updated
                                    })
                                } else {
                                    reject()
                                }
                            }
                            resolve(data)
                        }).catch((err) => {
                          //  console.log(err)
                            reject()
                        })
                    } else {
                        resolve([])
                    }
                }).catch((err) => {
                  //  console.log(err)
                    reject()
                })
            }).catch(() => {
                reject();
            })
        })
    }

    /*
    * Valida el id de una criptomoneda
    */
    ValidateIdCoin(id_coin: string) {
        return new Promise((resolve, reject) => {
            this.Ping().then(() => {
                Axios.get("https://api.coingecko.com/api/v3/coins/" + id_coin, { timeout: 1800 }).then((response) => {
                    resolve(true)
                }).catch((err) => {
                    reject(err.response.status)
                })
            }).catch(() => {
                reject();
            })
        })
    }

    /*
    * Valida un tipo de cambio en coingecko
    */
    ValidateCurrency(currency: string) {
        return new Promise((resolve, reject) => {
            this.Ping().then(() => {
                Axios.get("https://api.coingecko.com/api/v3/simple/supported_vs_currencies", { timeout: 1800 }).then((response) => {
                    resolve(response.data.indexOf(currency) >= 0)
                }).catch((err) => {
                    reject(err.response.status)
                })
            }).catch(() => {
                reject();
            })
        })

    }
}