import * as Sqlite3 from 'sqlite3';

export class Sqlite3Database {
    db: Sqlite3.Database;
    // variable para saber el estado de la base de datos
    connected: boolean = false;

    /**
     * En el constructor se crea la base de datos sqlite
     */
    constructor() {
        this.db = new Sqlite3.Database("data.sql", (err) => {
            if (err) {
                console.log('Could not connect to database', err)
            } else {
                this.connected = true;
                // console.log('Connected to database')
            }
        });

    }

    /**
     * Se crear las tablas si no estan creadas
     */
    create() {
        return new Promise((resolve, reject) => {
            this.db.serialize(() => {
                this.db.run(`
                                CREATE TABLE IF NOT EXISTS user 
                                (
                                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                                    name TEXT NOT NULL,
                                    lastname TEXT NOT NULL,
                                    currency TEXT NOT NULL,
                                    username TEXT NOT NULL,
                                    password TEXT NOT NULL
                                )`);
                this.db.run(`
                                CREATE TABLE IF NOT EXISTS userprofile 
                                (
                                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                                    id_user INTEGER,
                                    id_coin TEXT NOT NULL,
                                    foreign key(id_user) references user(id)
                                )`);

                this.db.run(`
                                CREATE TABLE IF NOT EXISTS auth 
                                (
                                    id INTEGER PRIMARY KEY AUTOINCREMENT, 
                                    id_user INTEGER,
                                    token TEXT NOT NULL,
                                    created_at datetime default current_timestamp,
                                    foreign key(id_user) references user(id)
                                )`);

                resolve(true);
            });
        });
    }

    /**
     * Funcion para realizar un "SELECT" en la base de datos
     */
    select(query: string, params: any[]) {
        return new Promise((resolve, reject) => {
            this.db.all(query, params, (err, data) => {
                if (err) {
                    reject(err);
                }
                resolve(data);
            })
        })
    }

    /**
     * Funcion para realizar ejecutar "UPDATE" o "INSERT" en la base de datos
     */
    excute(query: string, params: any[]) {
        return new Promise((resolve, reject) => {
            this.db.run(query, params, (err) => {
                if (err) {
                    console.log(err)
                }
                resolve(true);
            })
        })
    }
    /**
     * Funcion para cerrar la base de datos
     */
    close() {
        this.db.close()
    }

}