import * as bcrypt from 'bcrypt'
import * as jwt from 'jsonwebtoken';
import { AuthModel } from '../Models/AuthModel';
import { UserInterface } from '../Models/UserModel';


const authModel = new AuthModel();

export class Auth {
    constructor() {

    }

    /**
     * Funcion para comprobar usuario,contraseña del usuario y hacer login
     */
    login(pasword_request: string, user: UserInterface) {
        return new Promise((resolve, reject) => {
            bcrypt.compare(pasword_request, user.password).then((response: boolean) => {
                bcrypt.genSalt(parseInt(<any>process.env.BCRYPT_SALT_ROUNDS_TOKEN)).then((token_secret) => {
                    authModel.InsertToken({ token: token_secret, id_user: user.id }).then((responseDB) => {
                        resolve({
                            success: response,
                            token: (response ? "Bearer " + jwt.sign({ name: user.name, lastname: user.lastname, id: user.id, currency: user.currency }, token_secret, { expiresIn: <string>process.env.TOKEN_LIFETIME + "h" }) : "")
                        })
                    })
                })
            }).catch((error: any) => {
                console.log(error)
                reject(error)
            });
        });
    }

    /**
     * Funcion para comprobar el token del usuario
     */
    validate(token: string) {
        return new Promise((resolve, reject) => {
            token = token.replace('Bearer ', "")
            try {
                const user: any = jwt.decode(token);
                authModel.SelectByUser(user.id).then((responseDB: any) => {
                    const verified = jwt.verify(token, responseDB.token);
                    resolve({
                        success: true,
                        data: verified
                    })
                }).catch((err: any) => {
                    console.log(err)
                    reject({
                        success: false,
                        message: "invalid token"
                    })
                })
            } catch (error) {
                reject({
                    success: false,
                    message: "invalid token"
                })
            }
        });
    }
}