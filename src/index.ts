require('dotenv').config();

import { config } from 'dotenv/types';
import express from 'express'
import * as router from './api/Routes/Routes'
import { Sqlite3Database } from './api/Database/SqliteDatabase';

/*
 *Se crea la base de datos 
*/
let sql = new Sqlite3Database()
sql.create();


/*
 *Se crea la aplicacion express
*/
const app = express()

/*
 *Si se habilita el Access_Control_Allow_Origin en el archivo .env se permitira peticiones desde cualquier origen
*/
if (process.env.Access_Control_Allow_Origin == "true") {
    app.use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "*");
        res.header('Access-Control-Allow-Methods', "*");

        next();
    });
}

/*
 *Se agregan las rutas a la aplicacion de express
*/
app.use(router.router)


/*
 *Se exporta la aplicacion para test
*/
module.exports = app.listen(process.env.PORT, () => {
    console.log(`⚡️[server]: Server is running at http://localhost:${process.env.PORT}`);
})


