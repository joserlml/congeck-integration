# congeck-integration
> Pequeña API-REST con integración con el API de coingecko [https://www.coingecko.com/en/api#explore-api](https://www.coingecko.com/en/api#explore-api), usuario y login. Link repositorio: [https://gitlab.com/joserlml/congeck-integration](https://gitlab.com/joserlml/congeck-integration)

### Documentacion
* [Ejecucion del backend](#ejecucion-del-backend)
* [Ejecucion del test](#ejecucion-del-test)
* [Endpoint](#endpoint)
    * [Ingreso al sistema](#ingreso-al-sistema)
    * [Registro de usuario](#registro-de-usuario)
    * [Asignar criptomoneda a un usuario](#asignar-criptomoneda-a-un-usuario)
    * [Obtener criptomonedas disponibles](#obtener-criptomonedas-disponibles)
    * [Obtener criptomonedas segun configuracion del usuario](#obtener-criptomonedas-segun-configuracion-del-usuario)
    * [Archivo .env para configuracion](#archivo-env-para-configuracion)


#### Ejecucion del backend
Se tiene que instalar los siguientes paquetes:
- [npm](https://www.npmjs.com/)
- [nodejs](https://nodejs.org/en/download/)

Para ejecutarlo se tienen que seguir los siguientes pasos:

Descargar el repositorio del proyecto desde el siguiente enlace [https://gitlab.com/joserlml/congeck-integration](https://gitlab.com/joserlml/congeck-integration)
```sh
$ git clone https://gitlab.com/joserlml/congeck-integration
```
Desde una terminal en linux ingresar a la carpeta donde se decargo el repositorio y ejecutar el comando **npm -i**
```sh
$ cd congeck-integration
$ npm -i
```
Despues de haber terminado la ejecucion del comando anterior, ejecutar el comando **npm start** para ejecutar el backend 
```sh
$ npm start
```
Despues de haber ejecutado podras hacer uso del backend mediante [localhost:8001](localhost:8001)
>El puerto por defecto es el **8001** se puede cambiar desde el archivo **.env** en la configuracion **PORT**

#### Ejecucion del test
Se tiene que instalar los siguientes paquetes:
- [npm](https://www.npmjs.com/)
- [nodejs](https://nodejs.org/en/download/)

Para ejecutarlo se tienen que seguir los siguientes pasos:

Descargar el repositorio del proyecto desde el siguiente enlace [https://gitlab.com/joserlml/congeck-integration](https://gitlab.com/joserlml/congeck-integration)
```sh
$ git clone https://gitlab.com/joserlml/congeck-integration
```
Desde una terminal en linux ingresar a la carpeta donde se decargo el repositorio y ejecutar el comando **npm -i**
```sh
$ cd congeck-integration
$ npm -i
```
Despues de haber terminado la ejecucion del comando anterior, ejecutar el comando **npm test** para ejecutar los test 
```sh
$ npm test
```

#### Endpoint
##### Ingreso al sistema
```[POST]/api/login``` 

Mediante este endpoint podras ingresar al sistema
###### Request:
```json
{
    "username":"user",
    "password":"12345678"
}
```
###### Response(Status Code "200"):
```json
{
    "token":"Bearer eyJhbGc...."
}
```

##### Registro de usuario
```[POST]/api/user/register``` 

Mediante este endpoint podran registrar un usuario
###### Request:
```json
{
    "username":"user",
    "password":"12345678",
    "name":"nombre de usuario"
    "lastname":"apellido del usuario",
    "currency":"usd"
}
```
###### Response(Status Code "200"):
```json
{}
```

##### Asignar criptomoneda a un usuario
```[POST]/api/user/profile/coin``` 

Mediante este endpoint podran agregar criptomonedas a un usuario
###### Header request:
```json
{
    "Authorization": "TOKEN OBTENIDO DEL ENDPOINT DE LOGIN"
}
```
###### Request:
```json
{
    "id_coin": "cardano"
}
```
###### Response(Status Code "200"):
```json
{}
```

##### Obtener criptomonedas disponibles
```[GET]/api/coin/list/[[limit]]/[[ord]]``` 

Mediante este endpoint podran obtener la informacion de todas las criptomonedas disponibles

##### Parametros:

``` [[limit]]:``` Este parametro es la cantidad de criptomonedas que se quieren obtener.Por defecto todas

```[[ord]]```: Este parametro es para ordenar las criptomonedas de forma ascendente o descendente segun la capitalizacion actual. Los parametros aceptados son:
1) ```asc```: De forma ascendente
2) ```desc```: De forma descendente
> Si se envia un parametro no aceptado o no se envia alguno, por defecto de ordenara de forma descendente

###### Request:
Peticion de 5 criptomonedas en orden ascendente:
```url
http://localhost:8001/api/coin/list/5/asc
```
Peticion de 5 criptomonedas en orden descendente:
```url
http://localhost:8001/api/coin/list/5/desc
```
Peticion de 5 criptomonedas sin especificar un orden:
```url
http://localhost:8001/api/coin/list/5
```
Peticion de todas las criptomonedas sin especificar un orden:
```url
http://localhost:8001/api/coin/list
```
###### Response(Status Code "200"):
```json
[{
    "symbol": "btc",
    "name": "Bitcoin",
    "image": "https://assets.coingecko.com/coins/images/1/large/bitcoin.png?1547033579",
    "price": 35620,
    "last_updated": "2021-01-13T18:37:20.459Z"
},...]
```

##### Obtener criptomonedas segun configuracion del usuario
```[GET]/api/coin/user/list/[[limit]]/[[ord]]``` 

Mediante este endpoint podran obtener la informacion de las criptomonedas segun la configuracion del usuario

##### Parametros:

``` [[limit]]:``` Este parametro es la cantidad de criptomonedas que se quieren obtener

```[[ord]]```: Este parametro es para ordenar las criptomonedas de forma ascendente o descendente segun la capitalizacion actual. Los parametros aceptados son:
1) ```asc```: De forma ascendente
2) ```desc```: De forma descendente

> Si se envia un parametro no aceptado o no se envia alguno, por defecto de ordenara de forma descendente

###### Header request:
```json
{
    "Authorization": "TOKEN OBTENIDO DEL ENDPOINT DE LOGIN"
}
```
###### Request:
Peticion de 5 criptomonedas en orden ascendente:
```url
http://localhost:8001/api/coin/user/list/5/asc
```
Peticion de 5 criptomonedas en orden descendente:
```url
http://localhost:8001/api/coin/user/list/5/desc
```
Peticion de 5 criptomonedas sin especificar un orden:
```url
http://localhost:8001/api/coin/user/list/5
```
###### Response(Status Code "200"):
```json
[{
    "symbol": "btc",
    "usd": 35620,
    "eur": 29387,
    "ars": 3059287,
    "name": "Bitcoin",
    "image": "https://assets.coingecko.com/coins/images/1/large/bitcoin.png?1547033579",
    "last_updated": "2021-01-13T18:37:20.459Z"
},...]
```
#### Archivo env para configuracion
El archivo **.env** se encuentra en la raiz del proyecto este sirve para configurar el backend el cual contiene la siguientes configuraciones:
```bash
#
# Configuracion api rest
# 

 
# Puerto del servidor
PORT = 8001

# Permitir peticiones desde cualquier origen
Access_Control_Allow_Origin = true

# Salt para las contraseñas generadas
BCRYPT_SALT_ROUNDS_PASSWORD = 12
 
# Tiempo de vida del token en horas
TOKEN_LIFETIME = 24

# Salt para los token generados
BCRYPT_SALT_ROUNDS_TOKEN = 32
```