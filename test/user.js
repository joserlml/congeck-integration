module.exports = ({ request, should, Validator  }) => {
    describe('Pruebas con usuarios,login y asignacion de moneda', () => {
        var server;
        var tokenAuth;

        beforeEach(function () {
            server = require('../dist');
        });

        const tests = [
            {
                "url": "/api/user/register", 
                "name": "Se crea un usuario",
                "request": {
                    "username": "user",
                    "password": "12345678",
                    "name": "usuario",
                    "lastname": "apellido",
                    "currency": "usd"
                },
                "token": false,
                "response": {},
                "status": 200
            },
            {
                "url": "/api/user/register", 
                "name": "Se crea un usuario con un currency erroneo error 404",
                "request": {
                    "username": "user",
                    "password": "12345678",
                    "name": "usuario",
                    "lastname": "apellido",
                    "currency": "ussd"
                },
                "token": false,
                "response": {},
                "status": 404
            },
            {
                "url": "/api/user/register", 
                "name": "Se intenta crear un usuario con error 400",
                "request": {
                    "username": "user",
                    "password": "12345678",
                    "name": "usuario",
                    "lastname": "apellido"
                },
                "token": false,
                "response": {},
                "status": 400
            },
            {
                "url": "/api/user/register", 
                "name": "Se intenta crear un usuario con error 409",
                "request": {
                    "username": "user",
                    "password": "12345678",
                    "name": "usuario",
                    "lastname": "apellido",
                    "currency": "usd"
                },
                "token": false,
                "response": {},
                "status": 409
            },
            {
                "url": "/api/user/register", 
                "name": "Se intenta crear un usuario con contraseña de 7 caracateres con error 400",
                "request": {
                    "username": "user",
                    "password": "1234567",
                    "name": "usuario",
                    "lastname": "apellido",
                    "currency": "usd"
                },
                "token": false,
                "response": {},
                "status": 400
            },
            {
                "url": "/api/login", 
                "name": "Se hace login",
                "request": {
                    "username": "user",
                    "password": "12345678"
                },
                "schema": {
                    "type": "object",
                    "properties": {
                        'token': { "type": "string" },
                    },
                    "required": ["token"]
                },
                "token": false,
                "response": {},
                "status": 200
            },
            {
                "url": "/api/login", 
                "name": "Se hace login fallido con error 401",
                "request": {
                    "username": "user",
                    "password": "qwerty"
                },
                "token": false,
                "response": {},
                "status": 401
            },
            {
                "url": "/api/user/profile/coin", 
                "name": "Se agrega una moneda",
                "request": {
                    "id_coin": "bitcoin"
                },
                "token": true,
                "response": {},
                "status": 200
            },
            {
                "url": "/api/user/profile/coin", 
                "name": "Se intenta agregar una moneda que ya fue agregada",
                "request": {
                    "id_coin": "bitcoin"
                },
                "token": true,
                "response": {},
                "status": 409
            },
            {
                "url": "/api/user/profile/coin", 
                "name": "Se agrega una moneda que no existe",
                "request": {
                    "id_coin": "noexiste"
                },
                "token": true,
                "response": {},
                "status": 404
            },
            {
                "url": "/api/user/profile/coin", 
                "name": "Se intenta agregar una moneda sin haber hecho login",
                "request": {
                    "id_coin": "bitcoin"
                },
                "token": false,
                "response": {},
                "status": 401
            },
            {
                "url": "/api/user/profile/coin", 
                "name": "Se envia una peticion vacia",
                "request": {},
                "token": true,
                "response": {},
                "status": 400
            },
            {
                "url": "/api/user/profile/coin", 
                "name": "Se intenta crear una moneda que no existe",
                "request": {
                    "id_coin": "noexist"
                },
                "token": true,
                "response": {},
                "status": 404
            },
        ]
        for (const test of tests) {

            it(test.name, (done) => {
                request(server)
                    .post(test.url)
                    .send(test.request)
                    .set('Authorization', (test.token ? tokenAuth : "")) 
                    .expect('Content-Type', /json/)
                    .expect(test.status)
                    .expect((res) => {
                        if (test.schema) {
                            var validator = new Validator();
                            should.equal(validator.validate((Array.isArray(res.body) ? res.body[0] : res.body), test.schema).valid, true);
                        }
                    })
                    .expect((res) => {
                        if (res.body.token && test.status == 200) {
                            tokenAuth = res.body.token;
                            should.equal(res.body.success, true)

                        } else {
                            should.equal(JSON.stringify(res.body), JSON.stringify(test.response))
                        }
                    })
                    .end(function (err, res) {
                        if (err) throw err;
                        done();
                    });
            })
        }
    });

};