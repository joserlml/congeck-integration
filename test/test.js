const fs = require('fs');
const request = require('supertest');

const should = require('should')
const Validator = require('jsonschema').Validator;
const utils = { request, should, Validator };

describe('Pruebas al API', () => {
  before(function () {
    process.env.DISABLE_AUTH = "false"
    fs.stat('./data.sql', function (err, stats) {
      if (!err) {
        fs.unlinkSync('./data.sql');
      }
    });
  })

  require('./user')(utils);
  require('./coin')(utils);
});