module.exports = ({ request, should, Validator }) => {
    describe('Pruebas con obtencion de criptomonedas', () => {
        var server;
        var tokenAuth;

        beforeEach(function () {
            server = require('../dist');
        });

        const tests = [
            {
                "url": "/api/user/register",
                "name": "Se crea un usuario",
                "request": {
                    "username": "user2",
                    "password": "12345678",
                    "name": "usuario",
                    "lastname": "apellido",
                    "currency": "usd"
                },
                "token": false,
                "method": "post",
                "response": {},
                "status": 200
            },
            {
                "url": "/api/login",
                "name": "Se hace login",
                "request": {
                    "username": "user2",
                    "password": "12345678"
                },
                "schema": {
                    "type": "object",
                    "properties": {
                        'token': { "type": "string" },
                    },
                    "required": ["token"]
                },
                "token": false,
                "method": "post",
                "response": {},
                "status": 200
            },
            {
                "url": "/api/coin/list/3",
                "name": "Se intenta obtener ninguna moneda",
                "token": true,
                "method": "get",
                "length": 0,
                "status": 200
            },
            {
                "url": "/api/coin/list/5",
                "name": "Se intenta obtener 5 monedas sin hacer login",
                "token": false,
                "method": "get",
                "schema": {
                    "type": "object",
                    "properties": {
                        'symbol': { "type": "string" },
                        'price': { "type": "number" },
                        'name': { "type": "string" },
                        'image': { "type": "string" },
                        'last_updated': { "type": "string" },
                    },
                    "required": ["symbol", "price", "name", "image", "last_updated"]
                },
                "length": 5,
                "status": 200
            },
            {
                "url": "/api/coin/list/3",
                "name": "Se intenta obtener 3 monedas",
                "token": true,
                "method": "get",
                "schema": {
                    "type": "object",
                    "properties": {
                        'symbol': { "type": "string" },
                        'price': { "type": "number" },
                        'name': { "type": "string" },
                        'image': { "type": "string" },
                        'last_updated': { "type": "string" },
                    },
                    "required": ["symbol", "price", "name", "image", "last_updated"]
                },
                "length": 3,
                "status": 200
            },
            {
                "url": "/api/coin/list/50",
                "name": "Se intenta obtener 50 monedas",
                "token": true,
                "method": "get",
                "schema": {
                    "type": "object",
                    "properties": {
                        'symbol': { "type": "string" },
                        'price': { "type": "number" },
                        'name': { "type": "string" },
                        'image': { "type": "string" },
                        'last_updated': { "type": "string" },
                    },
                    "required": ["symbol", "price", "name", "image", "last_updated"]
                },
                "length": 50,
                "status": 200
            },
            {
                "url": "/api/coin/list/-1",
                "name": "Se intenta obtener -1 monedas",
                "token": true,
                "method": "get",
                "length": 0,
                "status": 400
            },
            {
                "url": "/api/coin/list/",
                "name": "Se intenta obtener todas las monedas",
                "token": true,
                "method": "get",
                "schema": {
                    "type": "object",
                    "properties": {
                        'symbol': { "type": "string" },
                        'price': { "type": "number" },
                        'name': { "type": "string" },
                        'image': { "type": "string" },
                        'last_updated': { "type": "string" },
                    },
                    "required": ["symbol", "price", "name", "image", "last_updated"]
                },
                "status": 200
            },
            {
                "url": "/api/coin/user/list/5",
                "name": "Se intenta top 5 monedas segun configuracion de usuario inexistente",
                "token": true,
                "method": "get",
                "length": 0,
                "status": 200
            },
            {
                "url": "/api/user/profile/coin",
                "name": "Se agrega 'bitcoin' como moneda del usuario",
                "request": {
                    "id_coin": "bitcoin"
                },
                "token": true,
                "method": "post",
                "response": {},
                "status": 200
            },
            {
                "url": "/api/user/profile/coin",
                "name": "Se agrega 'ethereum' como moneda del usuario",
                "request": {
                    "id_coin": "ethereum"
                },
                "token": true,
                "method": "post",
                "response": {},
                "status": 200
            },
            {
                "url": "/api/user/profile/coin",
                "name": "Se agrega 'tether' como moneda del usuario",
                "request": {
                    "id_coin": "tether"
                },
                "token": true,
                "method": "post",
                "response": {},
                "status": 200
            },
            {
                "url": "/api/user/profile/coin",
                "name": "Se agrega 'litecoin' como moneda del usuario",
                "request": {
                    "id_coin": "litecoin"
                },
                "token": true,
                "method": "post",
                "response": {},
                "status": 200
            },
            {
                "url": "/api/user/profile/coin",
                "name": "Se agrega 'cardano' como moneda del usuario",
                "request": {
                    "id_coin": "cardano"
                },
                "token": true,
                "method": "post",
                "response": {},
                "status": 200
            },
            {
                "url": "/api/coin/user/list/5",
                "name": "Se intenta top 5 monedas segun configuracion de usuario",
                "token": true,
                "method": "get",
                "schema": {
                    "type": "object",
                    "properties": {
                        'symbol': { "type": "string" },
                        'usd': { "type": "number" },
                        'eur': { "type": "number" },
                        'ars': { "type": "number" },
                        'name': { "type": "string" },
                        'image': { "type": "string" },
                        'last_updated': { "type": "string" },
                    },
                    "required": ["symbol", "usd", "eur", "ars", "name", "image", "last_updated"]
                },
                "length": 5,
                "status": 200
            },
            {
                "url": "/api/coin/user/list/0",
                "name": "Se intenta top 0 monedas segun configuracion de usuario",
                "token": true,
                "method": "get",
                "length": 0,
                "status": 200
            },
            {
                "url": "/api/coin/user/list/-1",
                "name": "Se intenta top 0 monedas segun configuracion de usuario",
                "token": true,
                "method": "get",
                "length": 0,
                "status": 400
            },
            {
                "url": "/api/coin/user/list/5/asc",
                "name": "Se intenta top 5 monedas en orden ascendente segun configuracion de usuario(sin comprobar orden)",
                "token": true,
                "method": "get",
                "schema": {
                    "type": "object",
                    "properties": {
                        'symbol': { "type": "string" },
                        'usd': { "type": "number" },
                        'eur': { "type": "number" },
                        'ars': { "type": "number" },
                        'name': { "type": "string" },
                        'image': { "type": "string" },
                        'last_updated': { "type": "string" },
                    },
                    "required": ["symbol", "usd", "eur", "ars", "name", "image", "last_updated"]
                },
                "length": 5,
                "status": 200
            },
            {
                "url": "/api/coin/user/list/5/desc",
                "name": "Se intenta top 5 monedas en orden descendente segun configuracion de usuario(sin comprobar orden)",
                "token": true,
                "method": "get",
                "schema": {
                    "type": "object",
                    "properties": {
                        'symbol': { "type": "string" },
                        'usd': { "type": "number" },
                        'eur': { "type": "number" },
                        'ars': { "type": "number" },
                        'name': { "type": "string" },
                        'image': { "type": "string" },
                        'last_updated': { "type": "string" },
                    },
                    "required": ["symbol", "usd", "eur", "ars", "name", "image", "last_updated"]
                },
                "length": 5,
                "status": 200
            },
        ]
        for (const test of tests) {
            if (test.method == "get") {
                it(test.name, (done) => {
                    request(server)
                        .get(test.url)
                        .send(test.request)
                        .set('Authorization', (test.token ? tokenAuth : ""))
                        .expect('Content-Type', /json/)
                        .expect(test.status)
                        .expect((res) => {
                            if (test.schema) {
                                var validator = new Validator();
                                should.equal(validator.validate((Array.isArray(res.body) ? res.body[0] : res.body), test.schema).valid, true);
                            }
                        })
                        .expect((res) => {
                            if (test.response) {
                                should.equal(JSON.stringify(res.body), JSON.stringify(test.response))
                            }
                            if (test.length) {
                                should.equal(res.body.length, test.length)
                            }
                        })
                        .end(function (err, res) {
                            if (err) throw err;
                            done();
                        });
                })
            } else if (test.method == "post") {

                it(test.name, (done) => {
                    request(server)
                        .post(test.url)
                        .send(test.request)
                        .set('Authorization', (test.token ? tokenAuth : ""))
                        .expect('Content-Type', /json/)
                        .expect(test.status)
                        .expect((res) => {
                            if (test.schema) {
                                var validator = new Validator();
                                should.equal(validator.validate((Array.isArray(res.body) ? res.body[0] : res.body), test.schema).valid, true);
                            }
                        })
                        .expect((res) => {
                            if (res.body.token && test.status == 200) {
                                tokenAuth = res.body.token;
                                should.equal(res.body.success, true)

                            } else {
                                should.equal(JSON.stringify(res.body), JSON.stringify(test.response))
                            }
                        })
                        .end(function (err, res) {
                            if (err) throw err;
                            done();
                        });
                })
            }

        }
    });

};